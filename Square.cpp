// Square.cpp: implementation of the Square class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Russia.h"
#include "Square.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Square::Square(int row,int col)
{
  //初始背景中的栅格：分别是它的行和列
  nRows = row;
  nCols = col;
  pDC=NULL;
  nScore=0;
  for(int i=0;i<row;i++)
    for(int m=0;m<col;m++)
      Grid[i][m]=0;
}

Square::~Square()
{

}

//cx: width, cy: height
void Square::DrawPicture(CDC *pDC,int x,int y,int cx,int cy,BOOL bGrid,BOOL bUpdate,COLORREF bcolorCell)
{
  nCellWidth = cx / nCols;
  nCellHeight = cy / nRows;

  CPen pen1,pen2;
  pen1.CreatePen(PS_SOLID,1,RGB(255,0,0));
  pen2.CreatePen(PS_SOLID,4,RGB(255,255,255));

  pDC->FillSolidRect(x,y,cx,cy,bcolorCell);

  if(bGrid)
  {  
    pDC->SelectObject(&pen1);
    for(int i=0;i<nCols;i++)
    {	
      pDC->MoveTo(x+i*nCellWidth,y);
      pDC->LineTo(x+i*nCellWidth,y+cy);
    }

    for( int k=0;k<nRows;k++)
    {	
      //	pDC->SelectObject(&pen2);
      pDC->MoveTo(x,y+k*nCellHeight);
      pDC->LineTo(x+cx,y+k*nCellHeight);
    }

    pDC->SelectObject(&pen2);
    //	 pDC->Rectangle(x,y,x+cx,y+cy);
    pDC->MoveTo(x,y);
    pDC->LineTo(x,y+cy);
    pDC->MoveTo(x,y+cy);
    pDC->LineTo(x+cx,y+cy);
    pDC->MoveTo(x+cx,y+cy);
    pDC->LineTo(x+cx,y);
    pDC->MoveTo(x+cx,y-2);
    pDC->LineTo(x,y-2);
  }  	   
}

void Square::DrawData(CDC* pDC,Cell celldata,int row,int col,COLORREF colorCell, int offsetY)
{     
  CString str;

  for(int r=0;r<celldata.row;r++)
    for(int c=0;c<celldata.col;c++)
    {    
      if(celldata.data[r][c]==1)
      {
        if(colorCell == RGB(0,250,140))
          pDC->FillSolidRect(11+(col+c)*nCellWidth, 11+(row+r) * nCellHeight + offsetY, nCellWidth-2,nCellHeight-2,colorCell);
        else
        {
          pDC->FillSolidRect(11+(col+c)*nCellWidth, 11+(row+r) * nCellHeight + offsetY, nCellWidth-2,nCellHeight-2,RGB(255,255,255));
          pDC->FillSolidRect(12+(col+c)*nCellWidth, 12+(row+r) * nCellHeight + offsetY, nCellWidth-4,nCellHeight-4,colorCell);
        }
      }
    }
}

void Square::EraseCell(CDC* pDC,int row,int col)
{
  for(int i=0;i<4;i++)
    for(int m=0;m<2;m++)
      pDC->FillSolidRect(11+(col+m)*nCellWidth,11+(row+i)*nCellHeight,nCellWidth-2,nCellHeight-2,RGB(0,0,0));
}

BOOL Square::CanDown(int row,int col,Cell* data)
{
  if(row + data->row >= nRows)
    return FALSE;   

  for(int m = data->row - 1; m >= 0; m--)
    for(int n=0;n<data->col;n++)
    {
      if(data->data[m][n]==1)
        if(Grid[row+m+1][col+n]==1)
          return FALSE;
    }
    return TRUE;
}

void Square::AddData(int row,int col,Cell* data)
{
  for(int m=0;m<data->row;m++)
    for(int n=0;n<data->col;n++)
    {	
      if(Grid[row+m][col+n]==0)
        Grid[row+m][col+n]=data->data[m][n];
    }		
}

BOOL Square::IsOver(int row,int col,Cell* data)
{
  for(int m = data->row; m >= 0; m--)
    for(int k = 0; k < data->col; k++) 
      if(Grid[m][k] == 1)
        return FALSE;

  if(Grid[15][8]==1)
    return FALSE;
  return TRUE;
}

BOOL Square::CanLeft(int row, int col, Cell* data)
{
  if(col <=0 )
    return FALSE;

  //这段代码都是一样的，在下落旋转过程中不能和已经被占用的格子发生碰撞
  for(int m = 0; m < data->col; m++)
    for(int n = 0; n < data->row; n++)
    {
      if(data->data[n][m] == 1)
        if(Grid[row+n][col-1+m] == 1)
          return FALSE;
    }

    return TRUE;
}

BOOL Square::CanRight(int row,int col,Cell* data)
{
  if(col+data->col-1>=15)
    return FALSE;
  for(int m=0;m<data->col;m++)
    for(int n=0;n<data->row;n++)
    {
      if(data->data[n][m]==1)
        if(Grid[row+n][col+1+m]==1)
          return FALSE;
    }
    return TRUE;
    return TRUE;
}

//判断能否消去一行
BOOL Square::IsFull(int row)
{
  for(int m=0;m<GAMEAREA_GRID_WIDTH;m++)
    if(Grid[row][m]==0)
      return FALSE;
  return TRUE;

}

void Square::ClearRow(int row,int col)
{
}

//消去一行并且得分，一行100分
BOOL Square::RemoveGrid()
{
  BOOL ret = false;
  for(int l=0;l < GAMEAREA_GRID_WIDTH;l++)
  {	 
    while(IsFull(l))
    {
      nScore += 100;
      MessageBeep(0);

      for (int i = l - 1; i >= 0; i--)
        for (int j = 0; j < GAMEAREA_GRID_WIDTH; j++)
          Grid[i+1][j] = Grid[i][j];

      ret=TRUE;
    }	
  }

  return ret;
}

BOOL Square::IsBarrierReachBoundary(){
  for(int r = 0; r < GAMEAREA_GRID_HEIGHT; ++r)
    if(Grid[r][0] == 1)
      return true;
  return false;
}

BOOL Square::RemoveBarrier()
{
  return TRUE;
}

void Square::DrawGrid(CDC *pDC,int row,int col,COLORREF GridCol,BOOL flag)
{
  if(flag)
  {
    pDC->FillSolidRect(11+(col)*nCellWidth,11+(row)*nCellHeight,nCellWidth-2,nCellHeight-2, RGB(255,255,255));
    pDC->FillSolidRect(12+(col)*nCellWidth,12+(row)*nCellHeight,nCellWidth-4,nCellHeight-4, GridCol);

  }
  else
    pDC->FillSolidRect(11+(col)*nCellWidth,11+(row)*nCellHeight,nCellWidth-2,nCellHeight-2, GridCol);
}

BOOL Square::CanTrun(int row,int col,Cell *data)
{
  if (data->row >data->col)
    if (col +data->row > nCols)
      return false;
  return true;

}

int Square::LifeValue()
{
  //	int ret=0;
  for(int i=0;i<GAMEAREA_GRID_HEIGHT;i++)
    for(int j=0;j<GAMEAREA_GRID_WIDTH;j++)
      if(Grid[i][j]==1)
      {		 
        //	return 2;
        return i;
        break;
      }

  return 0;
}

