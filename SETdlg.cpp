// SETdlg.cpp : implementation file
//

#include "stdafx.h"
#include "Russia.h"
#include "SETdlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SETdlg dialog


SETdlg::SETdlg(CWnd* pParent /*=NULL*/)
	: CDialog(SETdlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SETdlg)
	m_4 = 0;
	m_2 = 0;
	m_3 = 0;
	m_1 = _T("");
	//}}AFX_DATA_INIT
}


void SETdlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SETdlg)
	DDX_Text(pDX, IDC_4, m_4);
	DDX_Text(pDX, IDC_2, m_2);
	DDX_Text(pDX, IDC_3, m_3);
	DDX_Text(pDX, IDC_1, m_1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SETdlg, CDialog)
	//{{AFX_MSG_MAP(SETdlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SETdlg message handlers
