// RussiaDoc.cpp : implementation of the CRussiaDoc class
//

#include "stdafx.h"
#include "Russia.h"

#include "RussiaDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRussiaDoc

IMPLEMENT_DYNCREATE(CRussiaDoc, CDocument)

BEGIN_MESSAGE_MAP(CRussiaDoc, CDocument)
	//{{AFX_MSG_MAP(CRussiaDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRussiaDoc construction/destruction

CRussiaDoc::CRussiaDoc()
{
	// TODO: add one-time construction code here

}

CRussiaDoc::~CRussiaDoc()
{
}

BOOL CRussiaDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CRussiaDoc serialization

void CRussiaDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CRussiaDoc diagnostics

#ifdef _DEBUG
void CRussiaDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CRussiaDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRussiaDoc commands
