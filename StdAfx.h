// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__62A77E67_33DB_49DB_BFC7_B921B67BC540__INCLUDED_)
#define AFX_STDAFX_H__62A77E67_33DB_49DB_BFC7_B921B67BC540__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

//这句添加了控制台调试工具，工程属性/Linker/System/Subsystem这里看到
#ifdef _DEBUG
#pragma comment( linker, "/subsystem:console /entry:WinMainCRTStartup" )
#endif

//全局变量区
const int GAMEAREA_GRID_WIDTH = 16;
const int GAMEAREA_GRID_HEIGHT = GAMEAREA_GRID_WIDTH / 2;

const int TOTAL_GAMEAREA_WIDTH = 600;
const int TOTAL_GAMEAREA_HEIGHT = 400;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__62A77E67_33DB_49DB_BFC7_B921B67BC540__INCLUDED_)
