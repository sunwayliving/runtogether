// cell.cpp: implementation of the Cell class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Russia.h"
#include "cell.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Cell::Cell()
{
  //	row=4;
  //	col=2;
  for(int i=0;i<4;i++)
    for(int k=0;k<4;k++)
    {
      data[i][k]=0;
    }
    nControl=0;
}

Cell::~Cell()
{

}

void Cell::SetType(int nNumber)
{
  for(int j=0;j<4;j++)
    for(int k=0;k<4;k++)
      data[j][k]=0;

  switch(nNumber)
  {
  case 0:
    {
      data[0][0] = 1;
      data[1][0] = 1;
      data[2][0] = 1;
      data[3][0] = 1;
      data[0][1] = 0;
      data[1][1] = 0;
      data[2][1] = 0;
      data[3][1] = 0;
      col = 1;
      row = 4;
      break;
    }
  case 1:
    {
      data[0][0]=1;
      data[1][0]=0;
      data[0][1]=0;
      data[1][1]=0;
      data[2][0]=0;
      data[2][1]=0;
      data[3][0]=0;
      data[3][1]=0;
      col=1;
      row=1;
      break;
    }
  case 2:
    data[0][0]=1;
    data[1][0]=1;
    data[0][1]=0;
    data[1][1]=0;
    data[2][0]=0;
    data[2][1]=0;
    data[3][0]=0;
    data[3][1]=0;
    row=2;
    col=1;
    break;
  case 3:
    data[0][0]=1;
    data[0][1]=1;
    data[1][0]=1;
    data[1][1]=1;
    data[2][0]=0;
    data[2][1]=0;
    data[3][0]=0;
    data[3][1]=0;
    row=2;
    col=2;
    break;
  case 4:
    data[0][0]=1;
    data[1][0]=1;
    data[2][0]=1;
    data[3][0]=1;
    data[0][1]=0;
    data[1][1]=0;
    data[2][1]=0;
    data[3][1]=0;
    row=4;
    col=1;
    break;
  case 5:
    data[0][0]=1;
    data[1][0]=1;
    data[1][1]=1;
    data[2][1]=1;
    data[0][1]=0;
    data[2][0]=0;
    data[3][0]=0;
    data[3][1]=0;
    row=3;
    col=2;
    break;
  case 6:
    data[0][1]=1;
    data[1][0]=1;
    data[1][1]=1;
    data[2][0]=1;
    data[0][0]=0;
    data[2][1]=0;
    data[3][0]=0;
    data[3][1]=0;
    row=3;
    col=2;
    break;
  case 7:
    data[0][0]=1;
    data[1][0]=1;
    data[1][1]=1;
    data[2][0]=1;
    data[0][1]=0;
    data[2][1]=0;
    data[3][0]=0;
    data[3][1]=0;
    row=3;
    col=2;
    break;
  case 8:
    data[0][1]=1;
    data[1][0]=1;
    data[1][1]=1;
    data[2][1]=1;
    data[0][0]=0;
    data[2][0]=0;
    data[3][0]=0;
    data[3][1]=0;
    row=3;
    col=2;
    break;
  case 9:
    data[0][0]=1;
    data[0][1]=1;
    data[1][0]=1;
    data[2][0]=1;
    //	data[0][1]=0;
    data[1][1]=0;
    data[2][1]=0;
    data[3][0]=0;
    data[3][1]=0;
    row=3;
    col=2;
    break;
  case 10:
    data[0][0]=1;
    data[0][1]=1;
    data[1][1]=1;
    data[2][1]=1;
    //	data[0][1]=0;
    data[1][0]=0;
    data[2][0]=0;
    data[3][0]=0;
    data[3][1]=0;
    row=3;
    col=2;
    break;
  case 11:
    data[0][0]=1;
    data[1][0]=1;
    data[2][0]=1;
    data[2][1]=1;
    row=3;
    col=2;
    break;
  case 12:
    data[0][1]=1;
    data[1][1]=1;
    data[2][0]=1;
    data[2][1]=1;
    row=3;
    col=2;
    break;
  default:
    break;
  }
}

void Cell::Turn(Cell* data)
{
  int temp,nTemp;	
  nControl++;
  //	  CArray ar;
  int *Cata=new int[data->row * data->col];//(nControl%2)!=0
  if((nControl%2)!=0)
  {
    for(int i=0;i<data->row;i++)
      for(int j=0;j<data->col;j++)
        Cata[i*data->col+j]=data->data[i][j];		
    for(int m=0;m<data->row;m++)
      for(int n=0;n<data->col;n++)
        data->data[n][m]=Cata[m*data->col+n];

  }       
  else	
  { 
    nTemp=data->row;
    for(int j=0;j<data->row;j++)
      for(int k=0;k<data->col;k++)
        Cata[j*data->col+k]=data->data[j][k];
    for(int m=0;m<data->row;m++)
    {	
      nTemp--;
      for(int n=0;n<data->col;n++)
        data->data[n][nTemp]=Cata[m*data->col+n];
    }
  }
  temp=data->col;
  data->col=data->row;
  data->row=temp;
  delete Cata;
}