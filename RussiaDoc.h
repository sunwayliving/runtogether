// RussiaDoc.h : interface of the CRussiaDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_RUSSIADOC_H__0328D23C_D9AB_4442_8FA0_273A9AF555D8__INCLUDED_)
#define AFX_RUSSIADOC_H__0328D23C_D9AB_4442_8FA0_273A9AF555D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Cell.h"
class CRussiaDoc : public CDocument
{
protected: // create from serialization only
	CRussiaDoc();
	DECLARE_DYNCREATE(CRussiaDoc)

// Attributes
public:

// Operations
public:
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRussiaDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRussiaDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRussiaDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RUSSIADOC_H__0328D23C_D9AB_4442_8FA0_273A9AF555D8__INCLUDED_)
