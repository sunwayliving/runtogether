// Square.h: interface for the Square class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SQUARE_H__4CFC7695_C306_4D15_A202_C2D7062C1E53__INCLUDED_)
#define AFX_SQUARE_H__4CFC7695_C306_4D15_A202_C2D7062C1E53__INCLUDED_

#pragma once
#include "Cell.h"

class Square  
{
public:
	Square(int row ,int col);
	CDC* pDC;
	virtual ~Square();

	void DrawPicture(CDC* pDC,int x,int y,int cx,int cy,BOOL bGrid,BOOL bUpdate,COLORREF bcolorCell);	
	void DrawData(CDC* pDC,Cell celldata,int row,int col,COLORREF colorCell, int offsetY);
	void DrawGrid(CDC* pDC,int row,int col,COLORREF GridCol,BOOL flag=TRUE);
	void EraseCell(CDC* pDC,int row,int col);

	BOOL CanDown(int row,int col,Cell* data);
	BOOL CanLeft(int row,int col,Cell* data);
	BOOL CanRight(int row,int col,Cell* data);
	BOOL CanTrun(int row,int col,Cell* data);

	void AddData(int row,int col,Cell* data);
	BOOL IsOver(int row,int col,Cell* data);
	BOOL IsFull(int row);
	void ClearRow(int row,int col);
	int LifeValue();
  BOOL RemoveGrid();

  BOOL IsBarrierReachBoundary();
  BOOL RemoveBarrier();

	int Grid[GAMEAREA_GRID_HEIGHT][GAMEAREA_GRID_WIDTH];
	int nScore;
	int nCols,nRows;
	int nCellWidth;
	int nCellHeight;
};

#endif // !defined(AFX_SQUARE_H__4CFC7695_C306_4D15_A202_C2D7062C1E53__INCLUDED_)