// RussiaView.cpp : implementation of the CRussiaView class
//
#pragma  once
#include "stdafx.h"
#include "Russia.h"
#include "Square.h"
#include "RussiaDoc.h"
#include "RussiaView.h"
#include "Cell.h"
#include "SETdlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

Square win1(GAMEAREA_GRID_HEIGHT, GAMEAREA_GRID_WIDTH); //游戏区域被切分开的网格数量
Square win2(GAMEAREA_GRID_HEIGHT, GAMEAREA_GRID_WIDTH); //第二个游戏区域
/////////////////////////////////////////////////////////////////////////////
// CRussiaView

IMPLEMENT_DYNCREATE(CRussiaView, CView)

  BEGIN_MESSAGE_MAP(CRussiaView, CView)
    //{{AFX_MSG_MAP(CRussiaView)
    ON_COMMAND(ID_START, OnStart)
    ON_WM_TIMER()
    ON_COMMAND(ID_PAUSE, OnPause)
    ON_UPDATE_COMMAND_UI(ID_START, OnUpdateStart)
    ON_UPDATE_COMMAND_UI(ID_PAUSE, OnUpdatePause)
    ON_COMMAND(ID_EXIT, OnExit)
    ON_WM_DESTROY()
    ON_WM_CHAR()
    ON_COMMAND(ID_ONE, OnOne)
    ON_UPDATE_COMMAND_UI(ID_ONE, OnUpdateOne)
    ON_COMMAND(ID_TWO, OnTwo)
    ON_UPDATE_COMMAND_UI(ID_TWO, OnUpdateTwo)
    ON_COMMAND(ID_THREE, OnThree)
    ON_UPDATE_COMMAND_UI(ID_THREE, OnUpdateThree)
    ON_COMMAND(ID_FOUR, OnFour)
    ON_UPDATE_COMMAND_UI(ID_FOUR, OnUpdateFour)
    ON_COMMAND(ID_SETGRID, OnSetgrid)
    ON_UPDATE_COMMAND_UI(ID_SETGRID, OnUpdateSetgrid)
    ON_COMMAND(ID_DEFIEN, OnDefien)
    //}}AFX_MSG_MAP
    // Standard printing commands
    ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
    ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
    ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
  END_MESSAGE_MAP()

  /////////////////////////////////////////////////////////////////////////////
  // CRussiaView construction/destruction

  CRussiaView::CRussiaView()
  {
    // TODO: add construction code here
    bCircle1=TRUE;
    bCircle2 = TRUE;
    nType = 0; //nType=rand()%11;
    nSpeed=9;
    nKey=0;
    col1=7;
    row1=0;
    con1=0;

    row2 = 0;
    col2 = 0;
    con2 = 0;
    nScore=0;
    nLevel=1;
    nLife=15;
    bGrid=TRUE;
    bPause=FALSE;
    InitializeCriticalSection(&_critical);
  }

  CRussiaView::~CRussiaView()
  {
    DeleteCriticalSection(&_critical);
  }

  BOOL CRussiaView::PreCreateWindow(CREATESTRUCT& cs)
  {
    // TODO: Modify the Window class or styles here by modifying
    //  the CREATESTRUCT cs

    return CView::PreCreateWindow(cs);
  }

  /////////////////////////////////////////////////////////////////////////////
  // CRussiaView drawing

  void CRussiaView::OnDraw(CDC* pDC)
  {
    CRussiaDoc* pDoc = GetDocument();
    ASSERT_VALID(pDoc);
    // TODO: add draw code for native data here

    CRect rect;
    GetClientRect(rect);
    win1.DrawPicture(pDC, 1, 1, TOTAL_GAMEAREA_WIDTH, TOTAL_GAMEAREA_HEIGHT, FALSE, FALSE, RGB(0, 0, 0));//设置总的游戏区域的背景色, total game area

    //玩家1的游戏区域
    win1.DrawPicture(pDC, 10, 10, TOTAL_GAMEAREA_WIDTH / 3 * 2, TOTAL_GAMEAREA_HEIGHT / 2, bGrid,FALSE,RGB(0, 250, 140)); //设置方块掉落区域的背景色，并画出网格
    //玩家2的游戏区域
    win2.DrawPicture(pDC, 10, TOTAL_GAMEAREA_HEIGHT / 2, TOTAL_GAMEAREA_WIDTH / 3 * 2, TOTAL_GAMEAREA_HEIGHT / 2, bGrid, FALSE, RGB(0, 250, 140));
  }

  /////////////////////////////////////////////////////////////////////////////
  // CRussiaView printing

  BOOL CRussiaView::OnPreparePrinting(CPrintInfo* pInfo)
  {
    // default preparation
    return DoPreparePrinting(pInfo);
  }

  void CRussiaView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
  {
    // TODO: add extra initialization before printing
  }

  void CRussiaView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
  {
    // TODO: add cleanup after printing
  }

  /////////////////////////////////////////////////////////////////////////////
  // CRussiaView diagnostics

#ifdef _DEBUG
  void CRussiaView::AssertValid() const
  {
    CView::AssertValid();
  }

  void CRussiaView::Dump(CDumpContext& dc) const
  {
    CView::Dump(dc);
  }

  CRussiaDoc* CRussiaView::GetDocument() // non-debug version is inline
  {
    ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CRussiaDoc)));
    return (CRussiaDoc*)m_pDocument;
  }
#endif //_DEBUG
  /////////////////////////////////////////////////////////////////////////////
  // CRussiaView message handlers

  void CRussiaView::OnStart() 
  {
    // T;ODO: Add your command handler code here	
    bStart=FALSE;
    bPause=TRUE;

    CDC* pDC=GetDC();
    //玩家1的游戏区域
    win1.DrawPicture(pDC, 10, 10, 420, 197, bGrid, FALSE,RGB(0, 250, 140)); //设置方块掉落区域的背景色，并画出网格
    //玩家2的游戏区域
    win2.DrawPicture(pDC, 10, 197, 420, 197, bGrid, FALSE, RGB(0, 250, 140));
    ReleaseDC(pDC);

    nLife=15;
    start_time = clock();//开始游戏计时
    this->DplayScore();

    //设置定时器
    SetTimer(DISPLAYSCORE, 5, NULL);
    SetTimer(GAMEAREA, 5,NULL);
    //SetTimer(AREA2, 5, NULL);
  }

  void CRussiaView::OnTimer(UINT nIDEvent) 
  {
    // TODO: Add your message handler code here and/or call default

    switch(nIDEvent){
    case DISPLAYSCORE:
      DplayScore();
      break;
    case GAMEAREA:
      ProcessGameArea();
      break;
    default:
      break;
    }

    CView::OnTimer(nIDEvent);
    return;

    //if(!win.CanDown(rows,cols,&CeData))
    //{
    //  win.AddData(rows,cols,&CeData);
    //  if(win.RemoveGrid())
    //    for(int j=0;j < GAME_AREA_WIDTH;j++)
    //      for(int k=0;k < GAME_AREA_HEIGHT;k++)		
    //        if(win.Grid[j][k]==1)
    //          win.DrawGrid(win.pDC,j,k,RGB(0,0,255),TRUE);
    //        else
    //          win.DrawGrid(win.pDC,j,k,RGB(0,250,140),FALSE);

    //  nLife=win.LifeValue();
    //  DplayScore( win.nScore);
    //  this->SetLevel(win.nScore);
    //  bCircle=TRUE;
    //  return;
    //}else{
    //  switch(nKey)
    //  {
    //  case VK_SPACE:			
    //    if(win.CanDown(rows,cols,&CeData)){	 
    //      rows++;
    //      nKey=0;
    //    }
    //    break;
    //    //case key1:	
    //  case 'a':
    //  case 'A':
    //    if(win.CanLeft(rows,cols,&CeData))
    //    {	 
    //      cols--;
    //      nKey=0;
    //    }
    //    break;
    //  case 'F':
    //  case 'f':
    //    if(win.CanRight(rows,cols,&CeData))
    //    {
    //      cols++;
    //    }
    //    nKey=0;
    //    break;
    //  case 'd':
    //  case 'D':
    //    if(win.CanTrun(rows,cols,&CeData))
    //    {
    //      win.DrawData(win.pDC,CeData,OldRow,OldCol,RGB(0,250,140));
    //      CeData.Turn(&CeData);
    //      nKey=100;
    //    }
    //    nKey=0;
    //    break;
    //  }
    //}
  }

  void CRussiaView::OnPause() 
  {
    // TODO: Add your command handler code here
    bPause=FALSE;
    bStart=TRUE;
  }

  void CRussiaView::OnUpdateStart(CCmdUI* pCmdUI) 
  {
    // TODO: Add your command update UI handler code here
    pCmdUI->Enable(bStart);
  }

  void CRussiaView::OnUpdatePause(CCmdUI* pCmdUI) 
  {
    // TODO: Add your command update UI handler code here
    pCmdUI->Enable(bPause);
  }

  void CRussiaView::OnExit() 
  {
    // TODO: Add your command handler code here
    _exit(1);

    //	CView::OnDestroy();
  }

  void CRussiaView::OnDestroy() 
  {
    CView::OnDestroy();
    KillTimer(1);
    //TODO: Add your message handler code here

  }

  void CRussiaView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
  {
    // TODO: Add your message handler code here and/or call default
    nKey = nChar;
    CView::OnChar(nChar, nRepCnt, nFlags);
  }

  void CRussiaView::init()
  {
    bStart=TRUE;
    bPause=TRUE;

    for(int m=0;m<win1.nRows;m++)
      for(int n=0;n<win1.nCols;n++)
      {
        win1.Grid[m][n]=0;
      }
      row1=0;
      col1=7;
  }

  void CRussiaView::DplayScore()
  {
    CDC* pDC = GetDC();
    if (pDC == NULL)
      return;

    CString str;
    pDC->FillSolidRect(460,300,120,30,RGB(0,255,0));
    end_time = clock();
    long score = end_time - start_time;//根据时间差来计算得分
    str.Format("得分:%d",score);
    pDC->SetTextColor(RGB(255,255,255));
    pDC->TextOut(460,310,str);

    //pDC->FillSolidRect(460,360,120,30,RGB(0,0,255));
    ////	pen.CreatePen(PS_SOLID,3,RGB(255,255,255));
    //str.Format("水平:%d",nLevel);
    //pDC->SetTextColor(RGB(255,255,255));
    //pDC->TextOut(460,370,str);

    ReleaseDC(pDC);
  }

  void CRussiaView::OnOne() 
  {
    // TODO: Add your command handler code here
    nSpeed=9;
    nLevel=1;
  }

  void CRussiaView::OnUpdateOne(CCmdUI* pCmdUI) 
  {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetRadio(nSpeed==9);
  }

  void CRussiaView::OnTwo() 
  {
    // TODO: Add your command handler code here
    nSpeed=7;
    nLevel=2;
  }

  void CRussiaView::OnUpdateTwo(CCmdUI* pCmdUI) 
  {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetRadio(nSpeed==7);

  }

  void CRussiaView::OnThree() 
  {
    // TODO: Add your command handler code here
    nSpeed=5;
    nLevel=3;
  }

  void CRussiaView::OnUpdateThree(CCmdUI* pCmdUI) 
  {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetRadio(nSpeed==5);

  }

  void CRussiaView::OnFour() 
  {
    // TODO: Add your command handler code here
    nSpeed=3;
    nLevel=4;
  }

  void CRussiaView::OnUpdateFour(CCmdUI* pCmdUI) 
  {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetRadio(nSpeed==3);
  }

  void CRussiaView::OnSetgrid() 
  {
    // TODO: Add your command handler code here
    bGrid=!bGrid;
    win1.pDC=GetDC();
    win1.DrawPicture(win1.pDC,10,10,420,384,bGrid,FALSE,RGB(0, 250, 140));
    ReleaseDC(win1.pDC);
  }

  void CRussiaView::OnUpdateSetgrid(CCmdUI* pCmdUI) 
  {
    // TODO: Add your command update UI handler 你进\n第2关!code here
    pCmdUI->SetRadio(bGrid);
  }

  void CRussiaView::SetLevel(int Score)
  {
    CDC* pDC=GetDC();
    static BOOL b1,b2,b3,b4;
    CBrush brush,*pOldBrush;
    brush.CreateSolidBrush(RGB(255,255,255));
    pOldBrush=pDC->SelectObject(&brush);
    if(Score>=2000&&Score<2000)
    {    
      if(!b1){
        pDC->Ellipse(50,50,150,100);
        pDC->SetTextColor(RGB(255,0,0));
        pDC->TextOut(50,70,"恭喜进入2关");
        nSpeed=7;
        KillTimer(1);
        this->init();
        b1=TRUE;
      }
    }

    else if(Score>=2000&&Score<3000)
    {   
      if(!b2)
      {
        pDC->Ellipse(200,200,280,280);
        pDC->SetTextColor(RGB(255,255,255));
        pDC->TextOut(220,220,"恭喜你进\n第3关!");
        nSpeed=5;
        KillTimer(1);
        this->init();
        b2=TRUE;
      }

    }
    else if(Score>=3000)
    {
      if(!b3)
      {
        pDC->Ellipse(200,200,280,280);
        pDC->SetTextColor(RGB(255,255,255));
        pDC->TextOut(220,220,"恭喜你进\n第4关!");
        nSpeed=5;
        KillTimer(1);
        this->init();
        b3=TRUE;
      }
    }
    pDC->SelectObject(pOldBrush);
    ReleaseDC(pDC);
  }

  void CRussiaView::ProcessGameArea(const int areaNum)
  {
    if(!bPause)//设置暂停按钮
      return;

    int offsetY = TOTAL_GAMEAREA_HEIGHT / areaNum;
    
    CDC *pDC = GetDC();
    
    //处理玩家1
    {
      int	 oldRow1,oldCol;
      con1++;
      if(bCircle1)//产生下一个障碍
      {
        //当前障碍物产生的位置
        row1 = GAMEAREA_GRID_HEIGHT - BARRIER_HEIGHT;
        col1 = GAMEAREA_GRID_WIDTH - BARRIER_WIDTH;	
        curCell1.SetType(0);
        curCell1.nControl=0;
        //画出当前的障碍物
        win1.DrawData(pDC,curCell1,row1,col1,RGB(0,0,255), offsetY * 0);

        bCircle1=FALSE;
      }

      oldRow1 = row1;
      oldCol = col1;

      //在这里检测碰撞。(如果游戏区域的第一行上有格子则，游戏失败)
      for(int l = 0;l < GAMEAREA_GRID_WIDTH; l++)
      {
        if (win1.Grid[0][l]==1)
        {
          KillTimer(1);
          MessageBox("你输了!");
          this->init();
          win1.nScore=0;
          bCircle1=TRUE;
          return ;
        }
      }

      //障碍自动向左移
      if(con1 % nSpeed == 0 && win1.CanLeft(row1, col1, &curCell1)){
        col1--;
      }

      if(con1>=5000)
        con1=0;

      if(row1 != oldRow1 || nKey <= 0 || col1 != oldCol ||nKey == 100)
      {
        win1.DrawData(pDC,curCell1,oldRow1,oldCol,RGB(0, 250, 140), offsetY * 0);	
        win1.DrawData(pDC,curCell1,row1,col1,RGB(0,0,255), offsetY * 0);
      }

      //当移出屏幕之后，恢复背景色，且不重画当前的方块
      if(!win1.CanLeft(row1, col1, &curCell1)){
        win1.DrawData(pDC, curCell1, row1, col1, RGB(0, 250, 140), offsetY * 0);
        DplayScore();
        bCircle1 = true;//设定继续产生新的障碍物
        return;
      }
    }

    //处理玩家2
    {
      int oldRow2, oldCol2;
      con2++;
      if(bCircle2)//产生下一个障碍
      {
        //当前障碍物产生的位置
        row2 = GAMEAREA_GRID_HEIGHT - BARRIER_HEIGHT;
        col2 = GAMEAREA_GRID_WIDTH - BARRIER_WIDTH;	
        curCell2.SetType(0);
        curCell2.nControl=0;
        //画出当前的障碍物
        win2.DrawData(pDC, curCell2, row2, col2, RGB(0,0,255), offsetY * 1);
        bCircle2 = FALSE;
      }

      oldRow2 = row2;
      oldCol2 = col2;

      //在这里检测碰撞。(如果游戏区域的第一行上有格子则，游戏失败)
      for(int l = 0;l < GAMEAREA_GRID_WIDTH; l++)
      {
        if (win2.Grid[0][l]==1)
        {
          KillTimer(1);
          MessageBox("你输了!");
          this->init();
          win2.nScore=0;
          bCircle2=TRUE;
          return ;
        }
      }

      //障碍自动向左移
      if(con2 % nSpeed == 0 && win2.CanLeft(row2, col2, &curCell2)){
        col2--;
      }

      if(con2>=5000)
        con2=0;

      if(row2 != oldRow2 || nKey <= 0 || col2 != oldCol2 ||nKey == 100)
      {
        win2.DrawData(pDC,curCell2,oldRow2,oldCol2,RGB(0, 250, 140), offsetY * 1);	
        win2.DrawData(pDC,curCell2,row2,col2,RGB(0,0,255), offsetY * 1);
      }

      //当移出屏幕之后，恢复背景色，且不重画当前的方块
      if(!win2.CanLeft(row2, col2, &curCell2)){
        win2.DrawData(pDC, curCell2, row2, col2, RGB(0, 250, 140), offsetY * 1);
        DplayScore();
        bCircle2 = true;//设定继续产生新的障碍物
        return;
      }
    }

    ReleaseDC(pDC);
  }

  void CRussiaView::OnDefien() 
  {
    // TODO: Add your command handler code here
    //char t;
    //	SETdlg dlg;
    //	if(dlg.DoModal()==IDOK)
    //	{
    //	  key1=dlg.m_1;
    //	  key2=dlg.m_2;
    //	  key3=dlg.m_3;
    //	}
  }
