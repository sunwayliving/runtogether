// cell.h: interface for the Cell class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CELL_H__4954A29F_58A2_4E16_932F_75CDDCCC9B58__INCLUDED_)
#define AFX_CELL_H__4954A29F_58A2_4E16_932F_75CDDCCC9B58__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

const int BARRIER_HEIGHT = 4;
const int BARRIER_WIDTH = 2;

class Cell  
{
public:
	Cell();
	virtual ~Cell();
	int data[4][4];
	void SetType(int nNumber);
	int col,row;
	void Turn( Cell* data);
	int nControl;
};

#endif // !defined(AFX_CELL_H__4954A29F_58A2_4E16_932F_75CDDCCC9B58__INCLUDED_)
