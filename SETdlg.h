#if !defined(AFX_SETDLG_H__49F1BEB0_E25B_43D2_AAF4_28EFF19E6D7D__INCLUDED_)
#define AFX_SETDLG_H__49F1BEB0_E25B_43D2_AAF4_28EFF19E6D7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SETdlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// SETdlg dialog

class SETdlg : public CDialog
{
// Construction
public:
	SETdlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(SETdlg)
	enum { IDD = IDD_DIALOG1 };
	UINT	m_4;
	UINT	m_2;
	UINT	m_3;
	CString	m_1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(SETdlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(SETdlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SETDLG_H__49F1BEB0_E25B_43D2_AAF4_28EFF19E6D7D__INCLUDED_)
