//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Russia.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_RUSSIATYPE                  129
#define IDD_DIALOG1                     130
#define IDC_1                           1001
#define IDC_2                           1002
#define IDC_4                           1003
#define IDC_3                           1004
#define ID_START                        32772
#define ID_PAUSE                        32774
#define ID_EXIT                         32776
#define ID_ONE                          32777
#define ID_TWO                          32778
#define ID_THREE                        32779
#define ID_FOUR                         32780
#define ID_SETGRID                      32781
#define ID_DEFIEN                       32783

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32784
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
