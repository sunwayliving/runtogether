// RussiaView.h : interface of the CRussiaView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_RUSSIAVIEW_H__9C9C3365_DA22_4D9F_9655_EA82F6564AF0__INCLUDED_)
#define AFX_RUSSIAVIEW_H__9C9C3365_DA22_4D9F_9655_EA82F6564AF0__INCLUDED_

#pragma once
#include "Cell.h"
#include <iostream>
#include <time.h>

class Square;

class CRussiaView : public CView
{
public:
  enum{
    DISPLAYSCORE,
    GAMEAREA
  };

protected: // create from serialization only
	CRussiaView();

	Cell curCell1;
  Cell curCell2;

	int nKey;
	CString key1;
	CString key2;
	CString key3;
	int nScore;
  clock_t start_time;
  clock_t end_time;
	int row1, col1, row2, col2, con1, con2;
	int nLevel;
	int nLife;
	DECLARE_DYNCREATE(CRussiaView)

// Attributes
public:
	int Grid[GAMEAREA_GRID_HEIGHT][GAMEAREA_GRID_WIDTH];
	CRussiaDoc* GetDocument();
	void init();
  void DplayScore();
	void SetLevel(int Score);
  void ProcessGameArea(const int areaNum = 2);
  BOOL bCircle1;
  BOOL bCircle2;
	BOOL bGrid;
//	Cell celldata;
// Operations

public:
	int nType;
	int nSpeed;
	BOOL bStart;
	BOOL bPause;
  CRITICAL_SECTION _critical;
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRussiaView)

public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRussiaView();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	//{{AFX_MSG(CRussiaView)
	afx_msg void OnStart();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnPause();
	afx_msg void OnUpdateStart(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePause(CCmdUI* pCmdUI);
	afx_msg void OnExit();
	afx_msg void OnDestroy();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnOne();
	afx_msg void OnUpdateOne(CCmdUI* pCmdUI);
	afx_msg void OnTwo();
	afx_msg void OnUpdateTwo(CCmdUI* pCmdUI);
	afx_msg void OnThree();
	afx_msg void OnUpdateThree(CCmdUI* pCmdUI);
	afx_msg void OnFour();
	afx_msg void OnUpdateFour(CCmdUI* pCmdUI);
	afx_msg void OnSetgrid();
	afx_msg void OnUpdateSetgrid(CCmdUI* pCmdUI);
	afx_msg void OnDefien();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in RussiaView.cpp
inline CRussiaDoc* CRussiaView::GetDocument()
   { return (CRussiaDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RUSSIAVIEW_H__9C9C3365_DA22_4D9F_9655_EA82F6564AF0__INCLUDED_)
